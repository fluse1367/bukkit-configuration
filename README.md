# Bukkit Configuration API

This is a port of Bukkit's plugin configuration for universal usage.   
The Bukkit exclusive serialization types have been removed (such things as ItemStack, etc.).

All Copyright belongs to Bukkit: Copyright (c) 2020 Bukkit, licensed under
the [GNU General Public License v3.0](https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/raw/LICENCE.txt?at=8523fa23ad15bb668c9d2c4f259ee7d058ed1ff0))

- YAML-Configuration
  Package ([click](https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/configuration?at=8523fa23ad15bb668c9d2c4f259ee7d058ed1ff0))
- NumberConversions
  Class ([click](https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/util/NumberConversions.java?at=8523fa23ad15bb668c9d2c4f259ee7d058ed1ff0))

---

The API is published here:

<details><summary>Gradle</summary>

```groovy
// ...
repositories {
    // ...
    maven {
        url 'https://repo.software4you.eu/'
        // or url 'https://gitlab.com/api/v4/groups/software4you.eu/-/packages/maven/'
    }
    // ...
}
// ...
dependencies {
    // ...
    implementation 'org.bukkit:bukkit-configuration:1.0'
    // ...
}
// ...
```

</details>
<details><summary>Maven</summary>

```xml
<!-- ... -->
<repository>
    <id>software4you-repo</id>
    <url>https://repo.software4you.eu/</url>
    <!-- or <url>https://gitlab.com/api/v4/groups/software4you.eu/-/packages/maven/</url> -->
</repository>
        <!-- ... -->
<dependency>
<groupId>org.bukkit</groupId>
<artifactId>bukkit-configuration</artifactId>
<version>1.0</version>
</dependency>
        <!-- ... -->
```

</details>

**Attention:** This API depends on:

- <details><summary>SnakeYAML</summary>

  ```
      Full ID: org.yaml:snakeyaml:1.28
        Group: org.yaml
  Artifact ID: snakeyaml
      Version: 1.28
  ```
  </details>
- <details><summary>Apache Commons Lang 2</summary>

  ```
      Full ID: commons-lang:commons-lang:2.6
        Group: commons-lang
  Artifact ID: commons-lang
      Version: 2.6
  ```
  </details>